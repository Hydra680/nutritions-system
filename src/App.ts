import expres, {urlencoded, json} from 'express';
import morgan from 'morgan';
import cors from 'cors';




const app = expres();

//config server

app.set('port', process.env.PORT || 3000);

//middlewares
app.use(morgan('dev'));
app.use(cors());
app.use(urlencoded({extended: false}));
app.use(json());


//connection to db

//connectionDB();
//routes
app.get('/api', (req, res) => {
    res.json("Welcome to my API Nutritions Admin");
});

//app.use("/api/planetas",PlanetaRoutes);



export default app;
